import * as {{camelCase name }}Actions from '{{position "actions"}}/{{ lowerCase name }}.actions';

export interface State {
  loading: boolean;
  entities: { [id: string]: any };
  result: string[];
  error?: any;
}

export const initialState: State = {
  loading: false,
  entities: {},
  result: []
}

export function reducer(state = initialState, action: {{camelCase name }}Actions.{{ properCase name }}Actions): State {
  switch (action.type) {
    case {{camelCase name }}Actions.{{ properCase name }}Actions.LOAD.LOAD: {
      return {
        ...state,
        loading: true
      }
    }

    case {{camelCase name }}Actions.{{ properCase name }}Actions.LOAD.LOAD_SUCCESS: {

      return {
        ...state,
        loading: false,
      };
    }

     case {{camelCase name }}Actions.{{ properCase name }}Actions.LOAD.LOAD_FAIL: {

      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}