import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { {{ camelCase name }}Service } from '{{position "services"}}/{{ lowerCase name }}.service';
import * as {{ camelCase name }}Actions from '{{position "actions"}}/{{ lowerCase name }}.actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class {{ properCase name }}Effects {
  constructor(
    private {{ camelCase name }}Service: {{ properCase name }}Service,
    private actions$: Actions
  ) { }

  @Effect()
  get$ = this.actions$
    .ofType< {{ camelCase name }}Actions.{{ properCase name }}Actions>({{ camelCase name }}Actions.{{ properCase name }}Actions.LOAD)
    .map(toPayload)
    .switchMap(payload => {
      return this.{{ camelCase name }}Service.get(payload)
        .map(res => new {{properCase name }}Actions.Load{{ properCase name }}SuccessAction({payload: res.json}))
        .catch(error => Observable.of(new {{properCase name }}Actions.Load{{ properCase name }}ErrorAction({error: error})));
    });   
}