import { Action } from '@ngrx/store';

export const {{ constantCase name }}ActionTypes = {  
  LOAD: '[{{ titleCase name }}] Load',
  LOAD_ERROR: '[{{ titleCase name }}] Load Error',
  LOAD_SUCCESS: '[{{ titleCase name }}] Load Success',
}
/**
 * Load {{ titleCase name }} Actions
 */
export class Load{{ titleCase name }}Action implements Action {
  readonly type = {{ titleCase name }}ActionTypes.LOAD;

  constructor(public payload: any) { }
}

export class Load{{ titleCase name }}SuccessAction implements Action {
  readonly type = {{ titleCase name }}ActionTypes.LOAD_SUCCESS;

  constructor(public payload: any) { }
}

export class Load{{ titleCase name }}FailAction implements Action {
  readonly type = {{ titleCase name }}ActionTypes.LOAD_ERROR;

  constructor(public payload: any) { }
}

export type {{ titleCase name }}Actions =
  | Load{{ titleCase name }}Action
  | Load{{ titleCase name }}SuccessAction
  | Load{{ titleCase name }}FailAction;